alias phpsrv="php -S localhost:${PHP_DEV_SERVER_PORT}"
alias greprphp='grep -r -n --include=\*.php --color=always --exclude=\*.git\*'
