# Aliases associated with Brew, the OS X package manager.
if [ $OSX ]; then
    if (type gshred 1>/dev/null) 2>/dev/null && ! (type shred 1>/dev/null 2>/dev/null); then
        # gshrew was installed by brew, but shred and srm are not on the system by default
        alias shred="gshred"
    fi
fi
