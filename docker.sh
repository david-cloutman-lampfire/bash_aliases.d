# Shortcuts for Docker
alias dk='docker '
alias dk-psa='docker ps -a '
alias dk-lsi='docker images '
alias dk-lsc='docker container ls '
alias dk-execit='docker exec -it '
alias dk-start='docker start '
alias dk-rm='docker rm '
alias dk-rmi='docker rmi '
alias dk-logs='docker logs '
alias dk-runrmit='docker run -it --rm '
alias dk-psaexited='docker ps -a -f status="exited"'

alias dkc='docker-compose '
alias dkc-upd='docker-compose up -d'
alias dkc-dwn='docker-compose down'

alias dkpsa='dk-psa'

if [ $OS_X ]; then
    # For OS X, access the intermediate Docker host VM's shell.
    alias docker-vm-shell='docker run -it --rm --privileged --pid=host debian nsenter -t 1 -m -u -n -i sh'
fi


