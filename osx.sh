if [ $OSX ]; then
    #Recursively delete the dang .DS_Store turd files.
    alias rmrdsstore='find ./ -name .DS_Store -delete'

    # Flush the DNS cache in OS X.
    alias dnsflush="sudo dscacheutil -flushcache;sudo killall -HUP mDNSResponder;echo DNS cache has been flushed."

    # Get critical information about the operating system
    alias about-this-mac="sw_vers;echo;echo \"Environment Variables\";echo \"=====================\";env"

fi
