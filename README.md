# Cloutman's Bash Shell Aliases
Some of my personal favorite aliases from various systems I have worked on bundled into one convinient libary.

To load, call the main.sh where aliases are normally injected into your environment. (E.g. .bash_profile or .bash_aliases)

``` bash
source $HOME/bash_aliases.d/main.sh
```

## Adding Your Own Aliases
You may add your own alias files to this directory using the convention 'dev-[collectionname].sh'. All files in this directory that match the pattern dev-*.sh will automatically be executed as bash source.
