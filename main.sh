DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

# Load configurations here.
source "${DIR}/config.inc"

# These functions and aliases make changing and re-loading aliases easier.
function edaliases {
    DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
    if [ -f "${DIR}/${1}.sh" ]; then
        $EDITOR "${DIR}/$1.sh"
    else
        $EDITOR ~/.bash_aliases
    fi
}

alias upaliases='source ~/.bash_aliases'
alias testaliases='bash source ~/.bash_aliases'
alias lsaliases='ls ~/.bash_aliases.d'
function cataliases {
    DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
    if [ -f "${DIR}/${1}.sh" ]; then
        cat "${DIR}/$1.sh"
    else
        echo "File not found."
    fi
}

# If running in OS X, set up OS X aliases first.
## OS# #X only.
if [ $OSX ]; then
    source "${DIR}/osx.sh"
    if type brew 1>/dev/null 2>/dev/null; then
        source ${DIR}/brew.sh
    fi
fi


# Generally useful bash aliases.

# Save yourself at least two keystrokes.
alias lsla='ls -la '
alias psa='ps -A '
alias nw='nano -w '

# Easy shredding.
if type shred 1>/dev/null 2>/dev/null; then
    alias shred-it-ocd="shred -zfuv -n 255"
    alias shred-it-good="shred -zfuv -n 50"
    alias shred-it-fast="shred -zfuv -n 3"
    alias shred-it-real-fast="shred -zfuv -n 1"
fi

####################### LOAD LIBRARIES #######################

# For PHP users. We know you love PHP and we do not judge.
if type php 1>/dev/null 2>/dev/null; then
    source "${DIR}/php.sh"
fi

# Docker containers --require --verbose --flagage -to run .
if type docker 1>/dev/null 2>/dev/null; then
    source "${DIR}/docker.sh"
fi

# Include your custom alias files here. Custom alias file begin
# with 'dev-' and end with the '.sh' extension.
for filename in "${DIR}/dev-*.sh"; do source $filename; done
