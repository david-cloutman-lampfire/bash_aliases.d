# Set your configurations here. Remember that in bash 0 == true and 1 == false
EDITOR='nano -w '
OSX=0;

# PHP Specific
PHP_DEV_SERVER_PORT=8080

export EDITOR OSX PHP_DEV_SERVER_PORT
